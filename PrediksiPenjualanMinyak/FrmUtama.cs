﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using AForge.Neuro;
using AForge.Neuro.Learning;
using System.Collections;
using System.Threading;

namespace PrediksiPenjualanMinyak
{
    public partial class FrmUtama : Form
    {
        private double[][] dataTraining;
        private double[][] dataTrainingNormal;
        private double[][] dataTesting;
        private double[][] dataTestingNormal;
        private double[] dataTarget;
        private double[][] dataTargetNormal;
        private List<string> stringData;

        double learningRate = 0.0d;
        double momentum = 0.0d;
        int hiddenLayer = 1;
        int alpha = 2;
        int iterasi = 0;

        private ActivationNetwork network;

        ProgressDialog progressWindows;

        public FrmUtama()
        {
            InitializeComponent();
            progressWindows = new ProgressDialog();
            lblFile.Text = "";
            lblFileTesting.Text = "";
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            bool valid = true;

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                lblFile.Text = openFileDialog1.FileName;
                stringData = new List<string>();

                using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                {
                    while (!sr.EndOfStream)
                    {
                        String line = sr.ReadLine();
                        String[] item = line.Split(new char[] { ';' });
                        if (item.Length < 13)
                        {
                            MessageBox.Show("Format data tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                            break;
                        }
                        else
                        {
                            stringData.Add(line);
                        }
                    }
                }

                if (valid)
                {
                    dataTraining = new double[stringData.Count][];
                    dataTarget = new double[stringData.Count];
                    for (int i = 0; i < stringData.Count; i++)
                    {
                        string[] s = stringData[i].Split(new char[] { ';' });
                        dataTraining[i] = new double[12];

                        // validate input data
                        for (int j = 0; j < s.Length - 1; j++)
                        {
                            if (!Double.TryParse(s[j], out dataTraining[i][j]))
                            {
                                MessageBox.Show("Format data tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                valid = false;
                                break;
                            }
                        }

                        // validate target data
                        if (!Double.TryParse(s[12], out dataTarget[i]))
                        {
                            MessageBox.Show("Format data tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                            break;
                        }
                    }
                }

                if (valid)
                {
                    lstData.Items.Clear();
                    lstNormalisasi.Items.Clear();
                    dataTrainingNormal = new double[stringData.Count][];
                    dataTargetNormal = new double[stringData.Count][];
                    double[] temp = Normalization(dataTarget);
                    for (int i = 0; i < temp.Length; i++)
                    {
                        dataTargetNormal[i] = new double[] { temp[i] };
                    }

                    for (int i = 0; i < stringData.Count; i++)
                    {
                        string[] s = stringData[i].Split(new char[] { ';' });
                        lstData.Items.Add(new ListViewItem(s));
                        dataTrainingNormal[i] = Normalization(dataTraining[i]);

                        string[] item = new string[13];
                        for (int j = 0; j < 12; j++)
                            item[j] = dataTrainingNormal[i][j].ToString();

                        item[12] = dataTargetNormal[i][0].ToString();
                        lstNormalisasi.Items.Add(new ListViewItem(item));
                    }
                }

            }
        }

        private double[] Normalization(double[] data)
        {
            double[] temp = new double[data.Length];
            double min = data[0];
            double max = data[0];

            // get min and max value
            for (int i = 1; i < data.Length; i++)
            {
                if (data[i] < min)
                    min = data[i];

                if (data[i] > max)
                    max = data[i];
            }

            // start normalization
            for (int i = 0; i < data.Length; i++)
            {
                temp[i] = (0.8 * (data[i] - min) / (max - min)) + 0.1;
            }

            return temp;
        }

        public double TransformBack(double[] data, double val)
        {
            double min = data[0];
            double max = data[0];

            // get min and max value
            for (int i = 1; i < data.Length; i++)
            {
                if (data[i] < min)
                    min = data[i];

                if (data[i] > max)
                    max = data[i];
            }

            return ((val - 0.1) * (max - min) / 0.8) + min;
        }

        private void btnTraining_Click(object sender, EventArgs e)
        {

            if (!Double.TryParse(txtLearningRate.Text, out learningRate))
            {
                MessageBox.Show("Learning rate tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Double.TryParse(txtMomentum.Text, out momentum))
            {
                MessageBox.Show("Momentum tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Int32.TryParse(txtHidenLayer.Text, out hiddenLayer))
            {
                MessageBox.Show("Hidden layer tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Int32.TryParse(txtIteration.Text, out iterasi))
            {
                MessageBox.Show("Iterasi tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Thread workerThread = new Thread(new ThreadStart(Training));
            workerThread.Start();
            progressWindows.ShowDialog();

            //Training();
        }

        public void Training()
        {
            StringBuilder sb = new StringBuilder();
            progressWindows.SetIndeterminate(true);
            progressWindows.SetLabelMessage("Training Process");
            network = new ActivationNetwork(new SigmoidFunction(alpha), 12, hiddenLayer, 1);

            BackPropagationLearning teacher = new BackPropagationLearning(network);
            teacher.LearningRate = learningRate;
            teacher.Momentum = momentum;

            ArrayList errorsList = new ArrayList();
            int epoch = 1;
            while (epoch <= iterasi)
            {
                progressWindows.SetLabelMessage("Training Process - Epoch " + (epoch + 1));
                // run epoch of learning procedure
                double error = teacher.RunEpoch(dataTrainingNormal, dataTargetNormal);

                errorsList.Add(error);
                // check error value to see if we need to stop
                // ...
                Console.WriteLine(String.Format("Error {0}", error));

                sb.Append(String.Format("Epoch {0}   Output : {1}   Error : {2} \r\n", epoch, network.Output[0], error));
                if (error <= 0.1)
                {
                    break;
                }

                epoch++;
            }

            progressWindows.SetLabelMessage("Displaying Chart");
            this.BeginInvoke(new Action(() =>
            {
                chart.Series[0].Points.Clear();
                for (int i = 0; i < errorsList.Count; i++)
                {
                    chart.Series[0].Points.AddXY(i, (double)errorsList[i]);
                }
                txtLog.Text = sb.ToString();
            }));


            // Close the dialog if it hasn't been already
            if (progressWindows.InvokeRequired)
                progressWindows.BeginInvoke(new Action(() => progressWindows.Close()));
        }

        private void btnFileTesting_Click(object sender, EventArgs e)
        {
            DialogResult result = ofdTesting.ShowDialog();
            bool valid = true;

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                lblFile.Text = ofdTesting.FileName;
                stringData = new List<string>();

                using (StreamReader sr = new StreamReader(ofdTesting.FileName))
                {
                    while (!sr.EndOfStream)
                    {
                        String line = sr.ReadLine();
                        String[] item = line.Split(new char[] { ';' });
                        if (item.Length < 12)
                        {
                            MessageBox.Show("Format data tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                            break;
                        }
                        else
                        {
                            stringData.Add(line);
                        }
                    }
                }

                if (valid)
                {
                    dataTesting = new double[stringData.Count][];
                    for (int i = 0; i < stringData.Count; i++)
                    {
                        string[] s = stringData[i].Split(new char[] { ';' });
                        dataTesting[i] = new double[12];

                        // validate input data
                        for (int j = 0; j < s.Length - 1; j++)
                        {
                            if (!Double.TryParse(s[j], out dataTesting[i][j]))
                            {
                                MessageBox.Show("Format data tidak valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                valid = false;
                                break;
                            }
                        }
                    }
                }

                if (valid)
                {
                    lstDataTesting.Items.Clear();
                    lstNormalTesting.Items.Clear();
                    dataTestingNormal = new double[stringData.Count][];

                    for (int i = 0; i < stringData.Count; i++)
                    {
                        string[] s = stringData[i].Split(new char[] { ';' });
                        lstDataTesting.Items.Add(new ListViewItem(s));
                        dataTestingNormal[i] = Normalization(dataTesting[i]);
                    }
                }

            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            lstNormalTesting.Items.Clear();
            for (int i = 0; i < dataTestingNormal.Length; i++)
            {
                string[] stringTesting = new string[14];

                for (int j = 0; j < dataTestingNormal[i].Length; j++)
                {
                    stringTesting[j] = dataTestingNormal[i][j].ToString();
                }

                double result = network.Compute(dataTestingNormal[i])[0];
                stringTesting[12] = result.ToString();
                stringTesting[13] = TransformBack(dataTarget, result).ToString();

                lstNormalTesting.Items.Add(new ListViewItem(stringTesting));
            }
        }

        private void lstNormalTesting_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != lstNormalTesting) return;

            if (e.Control && e.KeyCode == Keys.C)
                CopySelectedValuesToClipboard(lstNormalTesting.SelectedItems);
        }

        private void CopySelectedValuesToClipboard(ListView.SelectedListViewItemCollection selectedListViewItemCollection)
        {
            var builder = new StringBuilder();
            foreach (ListViewItem item in selectedListViewItemCollection)
                builder.AppendLine(item.SubItems[1].Text);

            Clipboard.SetText(builder.ToString());
        }

        private void lstData_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != lstData) return;

            if (e.Control && e.KeyCode == Keys.C)
                CopySelectedValuesToClipboard(lstData.SelectedItems);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "CSV File|*.csv";
            saveFileDialog1.Title = "Save an CSV File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        StringBuilder sb = new StringBuilder();
                        foreach (ListViewItem item in lstNormalisasi.Items)
                        {
                            for (int i = 0; i < item.SubItems.Count; i++) {
                                sb.Append(item.SubItems[i].Text + ";");
                            }

                            sb.Append("\r\n");
                        }

                        byte[] info = new UTF8Encoding(true).GetBytes(sb.ToString());
                        fs.Write(info, 0, info.Length);
                        break;
                }

                fs.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "CSV File|*.csv";
            saveFileDialog1.Title = "Save an CSV File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        StringBuilder sb = new StringBuilder();
                        foreach (ListViewItem item in lstNormalTesting.Items)
                        {
                            for (int i = 0; i < item.SubItems.Count; i++)
                            {
                                sb.Append(item.SubItems[i].Text + ";");
                            }

                            sb.Append("\r\n");
                        }

                        byte[] info = new UTF8Encoding(true).GetBytes(sb.ToString());
                        fs.Write(info, 0, info.Length);
                        break;
                }

                fs.Close();
            }
        }
    }
}
