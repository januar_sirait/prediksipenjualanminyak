﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrediksiPenjualanMinyak
{
    class General
    {
        public static bool isDouble(string s, ref double r) {
            try {
                r = Double.Parse(s);
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }
    }
}
