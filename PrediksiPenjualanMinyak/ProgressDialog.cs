﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PrediksiPenjualanMinyak
{
    public partial class ProgressDialog : Form
    {
        public ProgressDialog()
        {
            InitializeComponent();
        }

        public void UpdateProgress(int progress)
        {
            if (progressBar.InvokeRequired)
                progressBar.BeginInvoke(new Action(() => progressBar.Value = progress));
            else
                progressBar.Value = progress;
        }

        public void SetLabelMessage(string text)
        {
            if (progressBar.InvokeRequired)
                progressBar.BeginInvoke(new Action(() => lblText.Text = text));
            else
                lblText.Text = text;
        }

        public void SetTitle(string text)
        {
            if (progressBar.InvokeRequired)
                progressBar.BeginInvoke(new Action(() => this.Text = text));
            else
                this.Text = text;
        }

        public void SetIndeterminate(bool isIndeterminate)
        {
            if (progressBar.InvokeRequired)
            {
                progressBar.BeginInvoke(new Action(() =>
                {
                    if (isIndeterminate)
                        progressBar.Style = ProgressBarStyle.Marquee;
                    else
                        progressBar.Style = ProgressBarStyle.Blocks;
                }
                ));
            }
            else
            {
                if (isIndeterminate)
                    progressBar.Style = ProgressBarStyle.Marquee;
                else
                    progressBar.Style = ProgressBarStyle.Blocks;
            }
        }
    }
}
