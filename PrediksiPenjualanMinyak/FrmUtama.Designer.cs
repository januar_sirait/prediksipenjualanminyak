﻿namespace PrediksiPenjualanMinyak
{
    partial class FrmUtama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtHidenLayer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIteration = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnTraining = new System.Windows.Forms.Button();
            this.txtMomentum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLearningRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFile = new System.Windows.Forms.Label();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lstNormalisasi = new System.Windows.Forms.ListView();
            this.NX1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NX12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NTarget = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstData = new System.Windows.Forms.ListView();
            this.X1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.X12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Target = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnProcess = new System.Windows.Forms.Button();
            this.lstNormalTesting = new System.Windows.Forms.ListView();
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.lblFileTesting = new System.Windows.Forms.Label();
            this.btnFileTesting = new System.Windows.Forms.Button();
            this.lstDataTesting = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ofdTesting = new System.Windows.Forms.OpenFileDialog();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(990, 667);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnExport);
            this.tabPage1.Controls.Add(this.txtLog);
            this.tabPage1.Controls.Add(this.chart);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.lblFile);
            this.tabPage1.Controls.Add(this.btnOpenFile);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.lstNormalisasi);
            this.tabPage1.Controls.Add(this.lstData);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(982, 641);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Training";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chart
            // 
            this.chart.BackColor = System.Drawing.Color.Gainsboro;
            chartArea3.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea3);
            legend3.DockedToChartArea = "ChartArea1";
            legend3.Name = "Legend1";
            this.chart.Legends.Add(legend3);
            this.chart.Location = new System.Drawing.Point(587, 252);
            this.chart.Name = "chart";
            series3.BorderWidth = 2;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.Name = "error";
            this.chart.Series.Add(series3);
            this.chart.Size = new System.Drawing.Size(387, 371);
            this.chart.TabIndex = 7;
            this.chart.Text = "chart1";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtHidenLayer);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtIteration);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnTraining);
            this.groupBox1.Controls.Add(this.txtMomentum);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtLearningRate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(587, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(387, 180);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameter";
            // 
            // txtHidenLayer
            // 
            this.txtHidenLayer.Location = new System.Drawing.Point(118, 25);
            this.txtHidenLayer.Name = "txtHidenLayer";
            this.txtHidenLayer.Size = new System.Drawing.Size(100, 20);
            this.txtHidenLayer.TabIndex = 8;
            this.txtHidenLayer.Text = "3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Hidden Layer";
            // 
            // txtIteration
            // 
            this.txtIteration.Location = new System.Drawing.Point(118, 99);
            this.txtIteration.Name = "txtIteration";
            this.txtIteration.Size = new System.Drawing.Size(100, 20);
            this.txtIteration.TabIndex = 6;
            this.txtIteration.Text = "10000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Itarasi";
            // 
            // btnTraining
            // 
            this.btnTraining.Location = new System.Drawing.Point(118, 125);
            this.btnTraining.Name = "btnTraining";
            this.btnTraining.Size = new System.Drawing.Size(75, 23);
            this.btnTraining.TabIndex = 4;
            this.btnTraining.Text = "Training";
            this.btnTraining.UseVisualStyleBackColor = true;
            this.btnTraining.Click += new System.EventHandler(this.btnTraining_Click);
            // 
            // txtMomentum
            // 
            this.txtMomentum.Location = new System.Drawing.Point(118, 73);
            this.txtMomentum.Name = "txtMomentum";
            this.txtMomentum.Size = new System.Drawing.Size(100, 20);
            this.txtMomentum.TabIndex = 3;
            this.txtMomentum.Text = "0.0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Momentum";
            // 
            // txtLearningRate
            // 
            this.txtLearningRate.Location = new System.Drawing.Point(118, 47);
            this.txtLearningRate.Name = "txtLearningRate";
            this.txtLearningRate.Size = new System.Drawing.Size(100, 20);
            this.txtLearningRate.TabIndex = 1;
            this.txtLearningRate.Text = "0.1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Learning Rate";
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(89, 18);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(35, 13);
            this.lblFile.TabIndex = 5;
            this.lblFile.Text = "label2";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(8, 13);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 4;
            this.btnOpenFile.Text = "Buka File";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Data Normalisasi";
            // 
            // lstNormalisasi
            // 
            this.lstNormalisasi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NX1,
            this.NX2,
            this.NX3,
            this.NX4,
            this.NX5,
            this.NX6,
            this.NX7,
            this.NX8,
            this.NX9,
            this.NX10,
            this.NX11,
            this.NX12,
            this.NTarget});
            this.lstNormalisasi.Location = new System.Drawing.Point(8, 252);
            this.lstNormalisasi.Name = "lstNormalisasi";
            this.lstNormalisasi.Size = new System.Drawing.Size(557, 180);
            this.lstNormalisasi.TabIndex = 2;
            this.lstNormalisasi.UseCompatibleStateImageBehavior = false;
            this.lstNormalisasi.View = System.Windows.Forms.View.Details;
            // 
            // NX1
            // 
            this.NX1.Text = "X1";
            // 
            // NX2
            // 
            this.NX2.Text = "X2";
            // 
            // NX3
            // 
            this.NX3.Text = "X3";
            // 
            // NX4
            // 
            this.NX4.Text = "X4";
            // 
            // NX5
            // 
            this.NX5.Text = "X5";
            // 
            // NX6
            // 
            this.NX6.Text = "X6";
            // 
            // NX7
            // 
            this.NX7.Text = "X7";
            // 
            // NX8
            // 
            this.NX8.Text = "X8";
            // 
            // NX9
            // 
            this.NX9.Text = "X9";
            // 
            // NX10
            // 
            this.NX10.Text = "x10";
            // 
            // NX11
            // 
            this.NX11.Text = "X11";
            // 
            // NX12
            // 
            this.NX12.Text = "X12";
            // 
            // NTarget
            // 
            this.NTarget.Text = "Target";
            // 
            // lstData
            // 
            this.lstData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.X1,
            this.X2,
            this.X3,
            this.X4,
            this.X5,
            this.X6,
            this.X7,
            this.X8,
            this.X9,
            this.X10,
            this.X11,
            this.X12,
            this.Target});
            this.lstData.Location = new System.Drawing.Point(8, 42);
            this.lstData.Name = "lstData";
            this.lstData.Size = new System.Drawing.Size(557, 180);
            this.lstData.TabIndex = 1;
            this.lstData.UseCompatibleStateImageBehavior = false;
            this.lstData.View = System.Windows.Forms.View.Details;
            this.lstData.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lstData_KeyUp);
            // 
            // X1
            // 
            this.X1.Text = "X1";
            // 
            // X2
            // 
            this.X2.Text = "X2";
            // 
            // X3
            // 
            this.X3.Text = "X3";
            // 
            // X4
            // 
            this.X4.Text = "X4";
            // 
            // X5
            // 
            this.X5.Text = "X5";
            // 
            // X6
            // 
            this.X6.Text = "X6";
            // 
            // X7
            // 
            this.X7.Text = "X7";
            // 
            // X8
            // 
            this.X8.Text = "X8";
            // 
            // X9
            // 
            this.X9.Text = "X9";
            // 
            // X10
            // 
            this.X10.Text = "x10";
            // 
            // X11
            // 
            this.X11.Text = "X11";
            // 
            // X12
            // 
            this.X12.Text = "X12";
            // 
            // Target
            // 
            this.Target.Text = "Target";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.btnProcess);
            this.tabPage2.Controls.Add(this.lstNormalTesting);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.lblFileTesting);
            this.tabPage2.Controls.Add(this.btnFileTesting);
            this.tabPage2.Controls.Add(this.lstDataTesting);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(982, 641);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Testing";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(11, 357);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 11;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lstNormalTesting
            // 
            this.lstNormalTesting.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.lstNormalTesting.Location = new System.Drawing.Point(8, 215);
            this.lstNormalTesting.Name = "lstNormalTesting";
            this.lstNormalTesting.Size = new System.Drawing.Size(966, 136);
            this.lstNormalTesting.TabIndex = 10;
            this.lstNormalTesting.UseCompatibleStateImageBehavior = false;
            this.lstNormalTesting.View = System.Windows.Forms.View.Details;
            this.lstNormalTesting.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lstNormalTesting_KeyUp);
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "X1";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "X2";
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "X3";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "X4";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "X5";
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "X6";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "X7";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "X8";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "X9";
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "X10";
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "X11";
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "X12";
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Hasil";
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Prediksi";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 188);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Result";
            // 
            // lblFileTesting
            // 
            this.lblFileTesting.AutoSize = true;
            this.lblFileTesting.Location = new System.Drawing.Point(89, 12);
            this.lblFileTesting.Name = "lblFileTesting";
            this.lblFileTesting.Size = new System.Drawing.Size(35, 13);
            this.lblFileTesting.TabIndex = 8;
            this.lblFileTesting.Text = "label2";
            // 
            // btnFileTesting
            // 
            this.btnFileTesting.Location = new System.Drawing.Point(8, 7);
            this.btnFileTesting.Name = "btnFileTesting";
            this.btnFileTesting.Size = new System.Drawing.Size(75, 23);
            this.btnFileTesting.TabIndex = 7;
            this.btnFileTesting.Text = "Buka File";
            this.btnFileTesting.UseVisualStyleBackColor = true;
            this.btnFileTesting.Click += new System.EventHandler(this.btnFileTesting_Click);
            // 
            // lstDataTesting
            // 
            this.lstDataTesting.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12});
            this.lstDataTesting.Location = new System.Drawing.Point(8, 36);
            this.lstDataTesting.Name = "lstDataTesting";
            this.lstDataTesting.Size = new System.Drawing.Size(966, 136);
            this.lstDataTesting.TabIndex = 6;
            this.lstDataTesting.UseCompatibleStateImageBehavior = false;
            this.lstDataTesting.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "X1";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "X2";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "X3";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "X4";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "X5";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "X6";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "X7";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "X8";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "X9";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "X10";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "X11";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "X12";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "CSV Files (*.csv)|*.csv";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // ofdTesting
            // 
            this.ofdTesting.Filter = "CSV Files (*.csv)|*.csv";
            this.ofdTesting.RestoreDirectory = true;
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(8, 438);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(557, 185);
            this.txtLog.TabIndex = 8;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(111, 224);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 9;
            this.btnExport.Text = "Export data";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(92, 357);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Export data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmUtama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 667);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FrmUtama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prediksi Penjualan Minyak Menggunakan Jaringan Saraf Tiruan Backpropagation";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lstData;
        private System.Windows.Forms.ColumnHeader X1;
        private System.Windows.Forms.ColumnHeader X2;
        private System.Windows.Forms.ColumnHeader X3;
        private System.Windows.Forms.ColumnHeader X4;
        private System.Windows.Forms.ColumnHeader X5;
        private System.Windows.Forms.ColumnHeader X6;
        private System.Windows.Forms.ColumnHeader X7;
        private System.Windows.Forms.ColumnHeader X8;
        private System.Windows.Forms.ColumnHeader X9;
        private System.Windows.Forms.ColumnHeader X10;
        private System.Windows.Forms.ColumnHeader X11;
        private System.Windows.Forms.ColumnHeader X12;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lstNormalisasi;
        private System.Windows.Forms.ColumnHeader NX1;
        private System.Windows.Forms.ColumnHeader NX2;
        private System.Windows.Forms.ColumnHeader NX3;
        private System.Windows.Forms.ColumnHeader NX4;
        private System.Windows.Forms.ColumnHeader NX5;
        private System.Windows.Forms.ColumnHeader NX6;
        private System.Windows.Forms.ColumnHeader NX7;
        private System.Windows.Forms.ColumnHeader NX8;
        private System.Windows.Forms.ColumnHeader NX9;
        private System.Windows.Forms.ColumnHeader NX10;
        private System.Windows.Forms.ColumnHeader NX11;
        private System.Windows.Forms.ColumnHeader NX12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMomentum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLearningRate;
        private System.Windows.Forms.Button btnTraining;
        private System.Windows.Forms.TextBox txtIteration;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.TextBox txtHidenLayer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ColumnHeader NTarget;
        private System.Windows.Forms.ColumnHeader Target;
        private System.Windows.Forms.Label lblFileTesting;
        private System.Windows.Forms.Button btnFileTesting;
        private System.Windows.Forms.ListView lstDataTesting;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ListView lstNormalTesting;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.OpenFileDialog ofdTesting;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button button1;

    }
}

