# README #

## Prediksi Penjualan Minyak dengan Menggunakan Jaringan Saraf Tiruan Backpropagation ##

### Requirement ###

* .Net Framework v.4
* Microsoft Visual Studio 2010 SP 1
* Nuget Package Manager (https://visualstudiogallery.msdn.microsoft.com/27077b70-9dad-4c64-adcf-c7cf6bc9970c)

### Installation ###

* Clone the project 
* Install Nuget Package Manager for Visual Studio 2010 from this link https://visualstudiogallery.msdn.microsoft.com/27077b70-9dad-4c64-adcf-c7cf6bc9970c
* Open Project Using Microsoft Visual Studio 2010
* Open menu **Tools > NuGet Package Manager > Package Manager Console**
* NuGet Console will show and enter this command 
**PM> Install-Package AForge.Neuro**